FROM debian:jessie

MAINTAINER  J.P.C. Oudeman

# Install nginx
RUN \
echo "deb http://nginx.org/packages/debian/ jessie nginx" > /etc/apt/sources.list.d/nginx.list && \
apt-key adv --fetch-keys http://nginx.org/packages/keys/nginx_signing.key && \
apt-get update && \
apt-get install -y nginx && \
\
# forward request and error logs to docker log collector
ln -sf /dev/stdout /var/log/nginx/access.log && \
ln -sf /dev/stderr /var/log/nginx/error.log && \
\
# Cleanup
apt-get clean -y && \
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["nginx", "-g", "daemon off;"]

WORKDIR /var/www/html